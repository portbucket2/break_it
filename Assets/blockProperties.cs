﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blockProperties : MonoBehaviour {

    public int blockStrength;
    public int maxblockStrength;
    public GameObject blockMesh;
    public GameObject cube;
    public ParticleSystem blockBreakParticle;
    public ParticleSystem blockSparkParticle;

    //public Color yellow;
    public Color[] blockColor;

    // Use this for initialization
    void Start () {

        blockStrength = Random.Range(1, maxblockStrength);
        
		
	}
	
	// Update is called once per frame
	void Update () {
		if(blockStrength == 0)
        {
            blockMesh.SetActive(false);
            blockBreakParticle.Play();
        }

        //cube.GetComponent<Renderer>().material.color =  Color.Lerp(yellow, green, blockStrength/5);
        ColorIt();

    }

    public void ruinStrength()
    {
        blockStrength -= 1;
        blockSparkParticle.Play();
    }

    void ColorIt()
    {
        cube.GetComponent<Renderer>().material.color = blockColor[blockStrength-1];
    }
}
