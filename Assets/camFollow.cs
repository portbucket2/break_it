﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camFollow : MonoBehaviour {

    public GameObject target;
    public Vector3 lowestPos;

	// Use this for initialization
	void Start () {
        lowestPos = new Vector3(0,8, 0);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        GetLowestPoint();
        transform.position = Vector3.Lerp(this.transform.position, lowestPos, Time.deltaTime * 5f);
		
	}

    void GetLowestPoint()
    {
        if(target.transform.position.y < lowestPos.y)
        {
            lowestPos.y = target.transform.position.y;
        }
    }
}
