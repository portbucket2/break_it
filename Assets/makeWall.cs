﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class makeWall : MonoBehaviour {

    public GameObject[] blocks;
    public GameObject[] spike;
    int blockID;
    public int maxBlocks;
    Vector3 blockPos;
    public int maxBreakableBlocks;
    public int level;

    
    public int breakablesOnWall;
    int numOfSpikes;

    GameObject gameController;


    // Use this for initialization
    void Start () {
        maxBreakableBlocks = 2;
        gameController = GameObject.FindGameObjectWithTag("GameController");
        level = gameController.GetComponent<gameController>().level;
        //MakeWall();
        MakeWall2();
        if(level > 2)
        {
            MakeMovingSpike();
        }
        
        

    }
	
	// Update is called once per frame
	void Update () {
        //level = gameController.GetComponent<gameController>().level;
    }

    void MakeWall()
    {
        blockPos = new Vector3(-4, this.transform.position.y, 0);
        for (int i = 0; i < maxBlocks; i++)
        {
            //Vector3 blockPos = new Vector3(-4, this.transform.position.y, 0);
            Instantiate(blocks[Random.Range(0, blocks.Length)], blockPos, transform.rotation);
            blockPos.x += 2;
        }
        
    }

    void MakeWall2()
    {
        blockPos = new Vector3(-4, this.transform.position.y, 0);
        for (int i = 0; i < maxBlocks; i++)
        {
            //Vector3 blockPos = new Vector3(-4, this.transform.position.y, 0);


            
            
            if (breakablesOnWall < 2 && i<3)
            {
                blockID = Random.Range(0, 2);
                if(blockID == 0)
                {
                    breakablesOnWall += 1;
                }
                //Debug.Log(i+"  " + blockID);
            }

            else if(i==3 && breakablesOnWall < 1)
            {
                blockID = 0;
                breakablesOnWall += 1;
                //Debug.Log("i3"+i + blockID);
            }
            
            else if (i == 4 && breakablesOnWall <2)
            {
                blockID = 0;
                breakablesOnWall += 1;
                //Debug.Log("i4"+i + blockID);
            }
            else
            {
                blockID = 1;
                //Debug.Log(i + "  " + blockID);
            }
            if (blockID == 1 && numOfSpikes < 2)
            {
                if(level == 2 || level >3)
                {
                    MakeSpike();
                }
                
            }
            Instantiate(blocks[blockID], blockPos, transform.rotation);
            blockPos.x += 2;

            
        }

    }
    void MakeSpike()
    {
        int spikeId = Random.Range(0, 2);
        Instantiate(spike[spikeId], blockPos, transform.rotation);
        numOfSpikes += 1;
        //blockPos = new Vector3(-4, this.transform.position.y, 0);
        //int numOfSpikes = 0;
        //for (int i = 0; i < maxBlocks; i++)
        //{
        //    int spikeId =  Random.Range(0, 2);
        //    if(spikeId == 0 && number of spikes < 2 )
        //    //Vector3 blockPos = new Vector3(-4, this.transform.position.y, 0);
        //    Instantiate(blocks[Random.Range(0, blocks.Length)], blockPos, transform.rotation);
        //    blockPos.x += 2;
        //}
    }

    void MakeMovingSpike()
    {
        Instantiate(spike[2], this.transform.position, transform.rotation);
    }


}
