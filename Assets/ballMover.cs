﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ballMover : MonoBehaviour {

    public float ballSpeed;
    float moveSpeed;
    public float mouseMoveX;
    public float ballPosX;

    Vector2 oldMousePos;
    Vector2 currMousePos;
    public Vector3 ballScale;
    public GameObject ballAnimHolder;
    public GameObject gameController;

    public bool isLevelEnded;



    // Use this for initialization
    void Start () {
        isLevelEnded = false;
        oldMousePos = new Vector2(0, 0);
	}
	
	// Update is called once per frame
	void Update () {
        //this.transform.Translate(moveSpeed * ballSpeed* 0.1f, 0, 0);
        //this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x,- 4.5f,4.5f),this.transform.position.y, this.transform.position.z);
        this.transform.position = new Vector3(Mathf.Clamp(Mathf.Lerp(this.transform.position.x,(ballPosX + mouseMoveX* ballSpeed),Time.deltaTime*20), -4.5f, 4.5f), this.transform.position.y, this.transform.position.z);
        Dragging();
        MaintainScale();
        if(this.GetComponent<Rigidbody>().velocity.y > -15f)
        {
            this.GetComponent<Rigidbody>().AddForce(0, -60f, 0);
        }
        else if(this.GetComponent<Rigidbody>().velocity.y <= -15f)
        {
            this.GetComponent<Rigidbody>().AddForce(0, -10f, 0);
        }



    }

    void Dragging()
    {
        if(Input.GetMouseButtonDown(0))
        {
            ballPosX = this.transform.position.x;
            oldMousePos = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            currMousePos = Input.mousePosition;
            //moveSpeed = currMousePos.x - oldMousePos.x;
            mouseMoveX = (currMousePos.x - oldMousePos.x)/ Screen.width;
            //oldMousePos = currMousePos;
        }
        if (Input.GetMouseButtonUp(0))
        {
            oldMousePos = currMousePos;
        }
        else
        {
            moveSpeed = 0;
        }
        
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    //this.GetComponent<Rigidbody>().velocity = new Vector3(0, 15, 0);
    //    //ballAnimHolder.transform.localScale = new Vector3(1, 0.7f, 1);
    //}
    private void OnTriggerEnter(Collider other)
    {
        this.GetComponent<Rigidbody>().velocity = new Vector3(0, 15, 0);
        ballAnimHolder.transform.localScale = new Vector3(1, 0.7f, 1);
        if (other.tag == "block")
        {
            other.gameObject.GetComponent<blockProperties>().ruinStrength();
        }
        if (other.tag == "spike")
        {
            this.gameObject.SetActive(false);
            SceneManager.LoadScene(0);
        }
        if (other.tag == "LevelEndBlock" && isLevelEnded == false)
        {
            gameController.GetComponent<gameController>().LevelEnd();
            isLevelEnded = true;
            //SceneManager.LoadScene(0);
        }
    }

    void MaintainScale()
    {
        ballAnimHolder.transform.localScale = Vector3.Lerp(ballAnimHolder.transform.localScale, new Vector3(1, 1.1f, 1), Time.deltaTime * 4f);
    }


}
