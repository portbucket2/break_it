﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameController : MonoBehaviour {

    public Vector3 wallSpawnPos;
    public float wallgap;
    public GameObject wallMaker;
    public GameObject levelEndBlock;
    public int maxWalls;

    public int level = 1 ;
    public GameObject levelEndUI;
    public Text levelText;
    public Text levelTextGame;


    // Use this for initialization
    void Start () {

        level = PlayerPrefs.GetInt("Level", level);
        levelEndUI.SetActive(false);
        UpdateLevelText();
        for (int i = 0; i < maxWalls; i++)
        {
            MakeLevel();
            wallSpawnPos.y -= wallgap;
        }

        Instantiate(levelEndBlock, wallSpawnPos, transform.rotation);
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    void MakeLevel()
    {
        Instantiate(wallMaker, wallSpawnPos, transform.rotation);
    }

    void UpdateLevel()
    {
        level += 1;
        UpdateLevelText();
        PlayerPrefs.SetInt("Level", level);
    }
    void UpdateLevelText()
    {
        levelText.text = "" + level;
        levelTextGame.text = "" + level;
    }
    public void ResetLevel()
    {
        level = 1;
        levelText.text = "" + level;
        PlayerPrefs.SetInt("Level", level);
    }
    public void LevelEnd()
    {
        levelEndUI.SetActive(true);
        UpdateLevel();

    }
    public void LevelStart()
    {
        SceneManager.LoadScene(0);

    }
}
